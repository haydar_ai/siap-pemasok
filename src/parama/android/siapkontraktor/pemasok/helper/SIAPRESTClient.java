package parama.android.siapkontraktor.pemasok.helper;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SIAPRESTClient {

	private static final String BASE_URL = "http://siap-kontraktor.com/android/api/v2/index.php";
	private static AsyncHttpClient mHttpClient = new AsyncHttpClient();

	public static void get(Context context, String URL, RequestParams params,
			AsyncHttpResponseHandler handler) {
		mHttpClient.get(context, getAbsoluteUrl(URL), params, handler);
	}

	public static void post(Context context, String URL, RequestParams params,
			AsyncHttpResponseHandler handler) {
		mHttpClient.post(context, getAbsoluteUrl(URL), null, params, "application/x-www-form-urlencoded", handler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}

}
