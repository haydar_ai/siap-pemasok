package parama.android.siapkontraktor.pemasok.helper;

/**
 * Created by Aldi on 10/22/13.
 */
public class ListMenu {

	private String title;
	private int icon;
	private boolean isSection = true;

	public ListMenu() {

	}

	public ListMenu(String title) {
		this.title = title;
	}

	public ListMenu(String title, int icon, boolean isSection) {
		super();
		this.title = title;
		this.icon = icon;
		this.isSection = isSection;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public boolean isSection() {
		return isSection;
	}

	public void setSection(boolean isSection) {
		this.isSection = isSection;
	}
}
