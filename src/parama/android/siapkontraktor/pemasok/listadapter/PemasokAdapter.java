package parama.android.siapkontraktor.pemasok.listadapter;

import java.util.ArrayList;

import parama.android.siapkontraktor.pemasok.MainActivity;
import parama.android.siapkontraktor.pemasok.R;
import parama.android.siapkontraktor.pemasok.model.Pemasok;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PemasokAdapter extends BaseAdapter {

	private Activity context;
	private ArrayList<Pemasok> listPemasok;
	
	public PemasokAdapter(Context context, ArrayList<Pemasok> listPemasok) {
		this.listPemasok = listPemasok;
	}
	
	public PemasokAdapter(MainActivity mainActivity, ArrayList<Pemasok> mListPemasok) {
		this.context = mainActivity;
		this.listPemasok = mListPemasok;
	}
	
	@Override
	public int getCount() {
		return listPemasok.size();
	}

	@Override
	public Object getItem(int arg0) {
		return listPemasok.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		Log.d("Adapter", "View");
		ViewHolder viewHolder = new ViewHolder();
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = arg1;
		if (arg1 == null) {
			view = inflater.inflate(R.layout.fragment_pemasok_item, null);
		}
		viewHolder.mNamaPemasok = (TextView) view.findViewById(R.id.namaPemasok);
		viewHolder.mNamaPemasok.setText(listPemasok.get(arg0).getNama());
		viewHolder.mNamaPemasok.setTextColor(Color.BLACK);
		viewHolder.mLayanan = (TextView) view.findViewById(R.id.namaLayanan);
		viewHolder.mLayanan.setText(listPemasok.get(arg0).getLayanan());
		viewHolder.mLayanan.setTextColor(Color.GRAY);
		return view;
	}
	
	public void setData(ArrayList<Pemasok> listPemasok) {
		this.listPemasok = listPemasok;
		notifyDataSetChanged();
	}
	
	private class ViewHolder {
		private TextView mNamaPemasok;
		private TextView mLayanan;
		public ViewHolder() {
			super();
		}
	}

}
