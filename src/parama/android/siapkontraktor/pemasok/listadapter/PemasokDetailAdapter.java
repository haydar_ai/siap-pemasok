package parama.android.siapkontraktor.pemasok.listadapter;

import java.util.ArrayList;

import parama.android.siapkontraktor.pemasok.DetailActivity;
import parama.android.siapkontraktor.pemasok.R;
import parama.android.siapkontraktor.pemasok.model.PemasokDetail;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class PemasokDetailAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<PemasokDetail> listPemasokDetail;
	private boolean editable;
	private ArrayList<String> mArrayList;
	private DataChangedListener dataChangedListener;

	public interface DataChangedListener {

		public void onDataChanged(ArrayList<String> arrayList);

	}

	public PemasokDetailAdapter(Context context,
			ArrayList<PemasokDetail> mListPemasokDetail, boolean editable,
			ArrayList<String> mArrayList,
			DataChangedListener dataChangedListener) {
		this.context = context;
		this.listPemasokDetail = mListPemasokDetail;
		this.editable = editable;
		this.mArrayList = mArrayList;
		this.dataChangedListener = dataChangedListener;
	}

	public PemasokDetailAdapter(Context context,
			ArrayList<PemasokDetail> mListPemasokDetail, boolean editable) {
		this.context = context;
		this.listPemasokDetail = mListPemasokDetail;
		this.editable = editable;
	}

	@Override
	public int getCount() {
		return listPemasokDetail.size();
	}

	@Override
	public Object getItem(int position) {
		return listPemasokDetail.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = new ViewHolder();
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = convertView;
		PemasokDetail pemasokDetail = (PemasokDetail) getItem(position);
		if (convertView == null) {
			view = inflater
					.inflate(R.layout.fragment_pemasok_detail_item, null);
		}
		viewHolder.mKey = (TextView) view.findViewById(R.id.key);
		viewHolder.mKey.setText(pemasokDetail.getKey());
		viewHolder.mKey.setTextColor(Color.BLACK);
		viewHolder.mValue = (EditText) view.findViewById(R.id.value);
		viewHolder.mValue.setText(pemasokDetail.getValue());
		viewHolder.mValue.setTextColor(Color.DKGRAY);
		viewHolder.mValue.setEnabled(editable);
		if (editable) {
			viewHolder.mValue.addTextChangedListener(new CTextWatcher(
					viewHolder.mValue, position));
		}

		return view;
	}

	public class CTextWatcher implements TextWatcher {

		View view;
		int position;

		public CTextWatcher(View view, int position) {
			this.view = view;
			this.position = position;
		}

		@Override
		public void afterTextChanged(Editable s) {
			mArrayList.set(position, s.toString());
			dataChangedListener.onDataChanged(mArrayList);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

	}

	public void setData(ArrayList<PemasokDetail> listPemasokDetail) {
		this.listPemasokDetail = listPemasokDetail;
		notifyDataSetChanged();
	}

	private static class ViewHolder {
		private TextView mKey;
		private TextView mValue;

		public ViewHolder() {
			super();
		}
	}
}
