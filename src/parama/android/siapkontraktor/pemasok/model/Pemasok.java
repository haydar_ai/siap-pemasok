package parama.android.siapkontraktor.pemasok.model;

public class Pemasok {

	String id;
	String kode;
	String buaso;
	String nama;
	String npwp;
	String kd_usaha;
	String bd_usaha;
	String kontak;
	String pimpinan;
	String jabatan;
	String akte;
	String alamat1;
	String alamat2;
	String kota;
	String tilpon1;
	String tilpon2;
	String fax1;
	String fax2;
	double utang;
	double u_awal;
	double u_baru;
	double u_bayar;
	double u_akhir;
	String tanda;
	String d_kunci;
	String d_input;
	String layanan;
	
	public String getLayanan() {
		return layanan;
	}
	
	public void setLayanan(String layanan) {
		this.layanan = layanan;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getKd_usaha() {
		return kd_usaha;
	}

	public void setKd_usaha(String kd_usaha) {
		this.kd_usaha = kd_usaha;
	}

	public String getBd_usaha() {
		return bd_usaha;
	}

	public void setBd_usaha(String bd_usaha) {
		this.bd_usaha = bd_usaha;
	}

	public String getKontak() {
		return kontak;
	}

	public void setKontak(String kontak) {
		this.kontak = kontak;
	}

	public String getPimpinan() {
		return pimpinan;
	}

	public void setPimpinan(String pimpinan) {
		this.pimpinan = pimpinan;
	}

	public String getJabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}

	public String getAlamat1() {
		return alamat1;
	}

	public void setAlamat1(String alamat1) {
		this.alamat1 = alamat1;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public double getUtang() {
		return utang;
	}

	public void setUtang(double utang) {
		this.utang = utang;
	}

}
