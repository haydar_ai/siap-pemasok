package parama.android.siapkontraktor.pemasok;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import parama.android.siapkontraktor.pemasok.helper.SIAPRESTClient;
import parama.android.siapkontraktor.pemasok.listadapter.PemasokAdapter;
import parama.android.siapkontraktor.pemasok.model.Pemasok;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MainActivity extends Activity {

	private ArrayList<Pemasok> mListPemasok;
	private PemasokAdapter mAdapterPemasok;
	private ListView mPemasokList;
	private JSONArray jarrayPemasok;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		String hostname = "202.78.200.65";
		String database = "siap002";
		String username = "admin";
		String password = "Siap123";

		RequestParams params = new RequestParams();
		params.put("hostname", hostname);
		params.put("database", database);
		params.put("username", username);
		params.put("password", password);

		sendLoginRequest(params);

		mListPemasok = new ArrayList<Pemasok>();
		mPemasokList = (ListView) findViewById(R.id.list);
		mPemasokList.setEmptyView(findViewById(R.id.progressBar));
		mAdapterPemasok = new PemasokAdapter(this, mListPemasok);
		mPemasokList.setAdapter(mAdapterPemasok);
		mPemasokList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent i = new Intent(MainActivity.this, DetailActivity.class);
				Bundle extras = new Bundle();
				try {
					String pemasok = jarrayPemasok.getJSONObject(position)
							.toString();
					extras.putString("pemasok", pemasok);
					extras.putInt("position", position);
					extras.putBoolean("boolean", true);
					i.putExtras(extras);
					startActivity(i);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void sendLoginRequest(RequestParams params) {
		SIAPRESTClient.post(MainActivity.this, "/user/login", params,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(JSONObject response) {
						try {
							boolean status = response.getBoolean("status");
							if (status) {
								Toast.makeText(MainActivity.this,
										"Login successfull", Toast.LENGTH_SHORT)
										.show();
								getData();
							} else {
								Toast.makeText(MainActivity.this,
										"Login error", Toast.LENGTH_SHORT)
										.show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
	}

	protected void getData() {
		SIAPRESTClient.get(MainActivity.this, "/pemasok/daftarPemasok", null,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(JSONObject response) {
						try {
//							Log.d("MainActivity",
//									"Response : " + response.toString());
							boolean status = response.getBoolean("status");
							if (status) {
								JSONArray data = response
										.getJSONArray("pemasok");
								jarrayPemasok = data;
								processData(data);
							} else {
								Toast.makeText(MainActivity.this,
										response.getString("message"),
										Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void processData(JSONArray data) throws JSONException {
		for (int i = 0; i < data.length(); i++) {
			Pemasok pemasok = new Pemasok();
			pemasok.setNama(data.getJSONObject(i).getString("nama"));
			pemasok.setLayanan(data.getJSONObject(i).getString("layanan"));
			mListPemasok.add(pemasok);
		}
		mAdapterPemasok.setData(mListPemasok);
	}
}
