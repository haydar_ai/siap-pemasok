package parama.android.siapkontraktor.pemasok;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.loopj.android.http.JsonHttpResponseHandler;

import parama.android.siapkontraktor.pemasok.helper.SIAPRESTClient;
import parama.android.siapkontraktor.pemasok.listadapter.PemasokDetailAdapter;
import parama.android.siapkontraktor.pemasok.model.PemasokDetail;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class DetailActivity extends SherlockActivity {

	private ArrayList<PemasokDetail> mListPemasokDetail;
	private PemasokDetailAdapter mAdapterPemasokDetail;
	private ListView mPemasokListDetail;
	private String mPemasok;
	private JSONArray jarrayPemasok;
	private int position;
	private Boolean bool;

	// private ArrayList<String> mArrayList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		mListPemasokDetail = new ArrayList<PemasokDetail>();
		mPemasokListDetail = (ListView) findViewById(R.id.list);
		mPemasokListDetail.setEmptyView(findViewById(R.id.progressBar));
		// mArrayList = new ArrayList<String>();

		Bundle extras = getIntent().getExtras();
		bool = extras.getBoolean("boolean");
		if (bool != false) {
			// Log.d("DetailActivity", "Response: " + mId);
			mPemasok = extras.getString("pemasok");
			position = extras.getInt("position");
			try {
				JSONObject jobj = new JSONObject(mPemasok);
				// Log.d("DetailAcitivty", "Response : " + jobj.toString());
				processData(jobj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			position = extras.getInt("position");
			String posInt = String.valueOf(position);
			Log.d("Position", posInt);
			getData();
		}
	}

	protected void getData() {
		SIAPRESTClient.get(DetailActivity.this, "/pemasok/daftarPemasok", null,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(JSONObject response) {
						try {
							Log.d("MainActivity",
									"Response : " + response.toString());
							boolean status = response.getBoolean("status");
							if (status) {
								JSONArray data = response
										.getJSONArray("pemasok");
								JSONObject obj = data.getJSONObject(position);
								mPemasok = obj.toString();
								processData(obj);
							} else {
								Toast.makeText(DetailActivity.this,
										response.getString("message"),
										Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void processData(JSONObject data) throws JSONException {
		JSONArray array = data.names();
		for (int i = 0; i < array.length(); i++) {
			String key = array.getString(i);
			String value = data.getString(key);
			key = key.replace("_", " ");
			key = key.substring(0, 1).toUpperCase(Locale.getDefault())
					+ key.substring(1);
			Integer find = key.indexOf(" ", 1);
			if (find != -1) {
				// Log.d("DetailActivity", find.toString());
				key = key.substring(0, find + 1)
						+ key.substring(find + 1, find + 2).toUpperCase(
								Locale.getDefault()) + key.substring(find + 2);
			}
			PemasokDetail pemasokDetail = new PemasokDetail(key, value);
			mListPemasokDetail.add(pemasokDetail);
			// mArrayList.add(value);
		}
		mAdapterPemasokDetail = new PemasokDetailAdapter(this,
				mListPemasokDetail, false);
		mPemasokListDetail.setAdapter(mAdapterPemasokDetail);
	}

//	private void processDataResume(JSONObject data) throws JSONException {
//		JSONArray array = data.names();
//		for (int i = 0; i < array.length(); i++) {
//			String key = array.getString(i);
//			String value = data.getString(key);
//			key = key.replace("_", " ");
//			key = key.substring(0, 1).toUpperCase(Locale.getDefault())
//					+ key.substring(1);
//			Integer find = key.indexOf(" ", 1);
//			if (find != -1) {
//				// Log.d("DetailActivity", find.toString());
//				key = key.substring(0, find + 1)
//						+ key.substring(find + 1, find + 2).toUpperCase(
//								Locale.getDefault()) + key.substring(find + 2);
//			}
//			PemasokDetail pemasokDetail = new PemasokDetail(key, value);
//			mListPemasokDetail.add(pemasokDetail);
//			// mArrayList.add(value);
//		}
//		mAdapterPemasokDetail = new PemasokDetailAdapter(this,
//				mListPemasokDetail, false);
//		mPemasokListDetail.setAdapter(mAdapterPemasokDetail);
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i = new Intent(DetailActivity.this, EditActivity.class);
		Bundle extras = new Bundle();
		extras.putString("pemasok", mPemasok);
		Log.d("Pemasokfuckyou", mPemasok);
		extras.putInt("position", position);
		i.putExtras(extras);
		switch (item.getItemId()) {
		case R.id.action_edit:
			startActivity(i);
			finish();
			return true;
		default:
			startActivity(i);
			finish();
			return true;
		}
	}
}